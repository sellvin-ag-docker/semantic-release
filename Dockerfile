FROM node:18-buster-slim
RUN apt-get update && apt-get install -y --no-install-recommends git-core ca-certificates
RUN npm install -g semantic-release @semantic-release/gitlab @semantic-release/exec
